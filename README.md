# Менеджер записи в несколько БД через MSMQ и DTC #

### Логи в Application и папке logs ###

## Механизм: ##
1. Создание задачи/сообщения xml для MSMQ
2. Добавление в очередь.
3. Открывается TransactionScope
4. Открывается MessageTransaction
5. Выполняется код на всех БД
6. В случае успеха все транзакции закрываются
7. В случае ошибки все транзакции откатываются а сообщение возвращается в очередь(**MessageTransaction**)

полученные xml обязательно проходят валидацию схемы xsd. во избежании всяких косячин

ОБЯЗАТЕЛЬНО ВКЛЮЧИТЬ В АВТО РЕЖИМ DTC - distributed transaction coordinator. Это служба windows. Она по умолчанию в manual

### Содержание ###


* Установка MSMQ
* Включение DTC
* Установка TransactionManager
* Проверка

### Установка MSMQ ###
![2016-08-11_054904.jpg](https://bitbucket.org/repo/LpARaz/images/2877174161-2016-08-11_054904.jpg)

#### DbTransactionUpdate ####

![2016-08-11_054957.jpg](https://bitbucket.org/repo/LpARaz/images/4198106846-2016-08-11_054957.jpg)


### Включение DTC ###

#### dcomcnfg ####

![2016-08-11_060945.jpg](https://bitbucket.org/repo/LpARaz/images/30093001-2016-08-11_060945.jpg)
![2016-08-11_054643.jpg](https://bitbucket.org/repo/LpARaz/images/4020879501-2016-08-11_054643.jpg)

### Установка TransactionManager ###

* запустить с правами админа install.bat

### Проверка ###

* отредактировать сообщения php_msmq/msg.xml и php_msmq/msg2.xml
* запустить скрипты php_msmq/add.php и php_msmq/remove.php

### Структура сообщения ###


```
#!xml

<root>
    <sql>
        **Код SQL который необходимо выполнить**
    </sql>

    <databases> - **Список БД**

        <database id="**Имя БД**" status="none"> - **БД**

            <server>**Сервер БД**</server>

            <dbname>**Имя БД**</dbname>

            <login>**логин БД**</login>

            <password>**Пароль БД**</password>

            <result> - **Статус , будет записан в папку logs**
                <status>
                         **success или error**
                </status>
                <date>
                          **дата**
                </date>
                <message>
                           **сообщение ошибки**
                </message>
            </result>
        </database>
        
    </databases>
</root>
```
### Пример ###

```
#!xml

<root>
    <sql>
        INSERT INTO [dbo].[Setting] ([Name], [Category], [Value]) VALUES (N'DefaultLang4', N'Localization', N'ru')
    </sql>

    <databases>
        <database id="rpsMDBG_CM" status="none">
            <server>localhost</server>
            <dbname>rpsMDBG_CM</dbname>
            <login>sa</login>
            <password>QWaszx!!</password>
            <result>
                <status>

                </status>
                <date>

                </date>
                <message>

                </message>
            </result>
        </database>
        <database id="Testovaya" status="none">
            <server>localhost</server>
            <dbname>Testovaya</dbname>
            <login>sa</login>
            <password>QWaszx!!</password>
            <result>
                <status>

                </status>
                <date>

                </date>
                <message>

                </message>
            </result>
        </database>
        <database id="TestScript" status="none">
            <server>localhost</server>
            <dbname>TestScript</dbname>
            <login>sa</login>
            <password>QWaszx!!</password>
            <result>
                <status>

                </status>
                <date>

                </date>
                <message>

                </message>
            </result>
        </database>
    </databases>
</root>
```