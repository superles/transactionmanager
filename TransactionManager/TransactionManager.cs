﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace TransactionManager
{
    class TransactionService
    {
        bool isRunning=false;

        private MessageQueue mq = new MessageQueue(@".\private$\DbTransactionUpdate");

        private XmlNode currentXmlNode;

        private XmlDocument msgXml = new XmlDocument();

        private XmlNodeList dbList;

        private string sql;

        private Message msg;

        private MessageQueueTransaction transaction = new MessageQueueTransaction();

        public TransactionService()
        {
            isRunning = true;

            //msgXml.Load("msg.xml");

            //mq.Send(new Message(msgXml.InnerXml),MessageQueueTransactionType.Single);

            
        }

        public void Start()
        {

            Thread Thread = new Thread(new ThreadStart(Loop));

            Thread.Start();
        }

        private SqlConnection MakeSqlConnection(XmlNode dbInfo)
        {
            
            string server = dbInfo.SelectSingleNode("server").InnerText;
            string dbname = dbInfo.SelectSingleNode("dbname").InnerText;
            string login = dbInfo.SelectSingleNode("login").InnerText;
            string password = dbInfo.SelectSingleNode("password").InnerText;

            return new SqlConnection(@"Data Source="+server+ @";Initial Catalog=" + dbname + ";user=" + login + ";password=" + password + "");
        }

        private void Loop()
        {
            isRunning = true;

            

            while (isRunning)
            {
                transaction.Begin();

                try
                {
                    msg = mq.Receive(TimeSpan.FromSeconds(10.0), transaction);

                    XmlSchemaSet schemas = new XmlSchemaSet();

                    var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);

                    var xsd = Path.Combine(outPutDirectory, "msg.xsd");

                    schemas.Add("", xsd);

                    msgXml.Schemas = schemas;

                    msgXml.Load(msg.BodyStream);



                    bool errors = false;

                    msgXml.Validate((o, e) =>
                    {
                        Console.WriteLine("{0}", e.Message);
                        errors = true;
                    });

                    if (!errors)
                    {


                        XmlNode root = msgXml.SelectSingleNode(@"root");

                        sql = root.SelectSingleNode("sql").InnerText;

                        dbList = root.SelectSingleNode("databases").SelectNodes("database");



                        int unixTime = (int) (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

                        Run();

                        string xml =
                            Path.Combine(
                                new Uri(Path.GetDirectoryName(Assembly.GetEntryAssembly().CodeBase)).LocalPath,
                                @"logs\\DbUpdate_" + unixTime.ToString() + @".xml");

                        msgXml.Save(xml);
                    }
                    else
                    {
                        transaction.Abort();
                    }


                }
                catch (System.Messaging.MessageQueueException)
                {
                    transaction.Abort();


                }

                

                
                
                
                
                

                

                

                Thread.Sleep(5000);
            }
        }
        private void Run()
        {
            using (TransactionScope ts2 = new TransactionScope())
            {
                try
                {

                    foreach (XmlNode dbInfo in dbList)
                    {
                        currentXmlNode = dbInfo;

                        using (SqlConnection connection = MakeSqlConnection(dbInfo) )
                        {
                            //SqlCommand cmd = new SqlCommand("INSERT INTO [dbo].[Setting] ([Name], [Category], [Value]) VALUES (N'DefaultLang4', N'Localization', N'ru')");
                            SqlCommand cmd = new SqlCommand(sql);
                            cmd.Connection = connection;
                            cmd.Connection.Open();
                            cmd.ExecuteNonQuery();

                            dbInfo.SelectSingleNode("result").SelectSingleNode("status").InnerText = "success";
                            dbInfo.SelectSingleNode("result").SelectSingleNode("date").InnerText =
                                DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                            dbInfo.Attributes["status"].Value = "success";
                        }
                    }


                    transaction.Commit();

                    ts2.Complete();


                }
                catch (TransactionAbortedException tax)
                {
                    using (EventLog eventLog = new EventLog("Application"))
                    {
                        eventLog.Source = "Application";
                        eventLog.WriteEntry("TransactionAbortedException Event "+tax.Message, EventLogEntryType.Information, 101, 1);
                    }

                    transaction.Abort();
                }
                catch (Exception ex)
                {
                    using (EventLog eventLog = new EventLog("Application"))
                    {
                        eventLog.Source = "Application";
                        eventLog.WriteEntry(" Ошибка обработки очереди." +
                                            "\r\n Сервер: " + currentXmlNode.SelectSingleNode("server").InnerText +
                                            "\r\n БД: " + currentXmlNode.SelectSingleNode("dbname").InnerText +
                                            "\r\n Id сообщения:"+msg.Id+" " +
                                            "\r\n Ошибка:" + ex.Message, EventLogEntryType.Error);
                    }

                    currentXmlNode.SelectSingleNode("result").SelectSingleNode("status").InnerText = "error";
                    currentXmlNode.SelectSingleNode("result").SelectSingleNode("date").InnerText =
                        DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                    currentXmlNode.SelectSingleNode("result").SelectSingleNode("message").InnerText = ex.Message.ToString(); ;

                    currentXmlNode.Attributes["status"].Value = "error";

                    transaction.Abort();
                }
                
            }
        }

        public void Stop()
        {
            isRunning = false;
        }
    }
}
