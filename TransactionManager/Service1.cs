﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TransactionManager
{
    public partial class Service1 : ServiceBase
    {
        private TransactionService ts = new TransactionService();
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            
            ts.Start();
        }

        protected override void OnStop()
        {
         
            ts.Stop();
        }
    }
}
