<?php

defined("MQ_SEND_ACCESS") or define("MQ_SEND_ACCESS" , 2);
defined("MQ_DENY_NONE") or define("MQ_DENY_NONE" , 0);
defined("TransactionType") or define("TransactionType" , 3);

$msgQueueInfo =  new COM("MSMQ.MSMQQueueInfo") or die("can not create MSMQ Info object");
$msgQueueInfo->PathName = ".\private$\DbTransactionUpdate";

        
$msgQueue =  new COM("MSMQ.MSMQQueue") or die("can not create MSMQ object"); 
$msgQueue=$msgQueueInfo->Open(MQ_SEND_ACCESS, MQ_DENY_NONE ); 

$msgOut = new COM("MSMQ.MSMQMessage") or die("can not create MSMQ message object");                

$msgOut->Body = file_get_contents('msg.xml');


$msgOut->Send($msgQueue,TransactionType);
        
$msgQueue->Close();     

?>